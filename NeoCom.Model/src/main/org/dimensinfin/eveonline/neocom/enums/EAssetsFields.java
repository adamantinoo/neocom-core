//  PROJECT:        BlueprintListProof
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package org.dimensinfin.eveonline.neocom.enums;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum EAssetsFields {
	NAME, CATEGORY, GROUP, COUNT, META, REGION, TECH, TYPEID
}

// - UNUSED CODE ............................................................................................
