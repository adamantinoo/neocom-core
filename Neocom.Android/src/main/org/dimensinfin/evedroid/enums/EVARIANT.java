//	PROJECT:        NeoCom.Android (NEOC.A)
//	AUTHORS:        Adam Antinoo - adamantinoo.git@gmail.com
//	COPYRIGHT:      (c) 2013-2016 by Dimensinfin Industries, all rights reserved.
//	ENVIRONMENT:		Android API16.
//	DESCRIPTION:		Application to get access to CCP api information and help manage industrial activities
//									for characters and corporations at Eve Online. The set is composed of some projects
//									with implementation for Android and for an AngularJS web interface based on REST
//									services on Sprint Boot Cloud.
package org.dimensinfin.evedroid.enums;

// - IMPORT SECTION .........................................................................................

// - M O D E L   V I E W   C O N T R O L L E R   V A R I A N T S
public enum EVARIANT {
	DEFAULT_VARIANT, CAPSULEER_LIST, FITTING_LIST, FITTING_MODULES, FITTING_MANUFACTURE, SHIPS_BYLOCATION, SHIPS_BYCLASS
}
// - UNUSED CODE ............................................................................................
