//  PROJECT:        DroidModel
//  AUTHORS:        Adam Antinoo - haddockgit@gmail.com
//  COPYRIGHT:      (c) 2013-2014 by Dimensinfin Industries, all rights reserved.

package org.dimensinfin.evedroid.core;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum ERequestState {
	EMPTY, NEEDSUPDATE, UPDATED, ON_PROGRESS, ERROR, NOT_FOUND, COMPLETED, PENDING
}

// - UNUSED CODE ............................................................................................
