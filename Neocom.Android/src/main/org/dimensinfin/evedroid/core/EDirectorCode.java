//  PROJECT:        DroidModel
//  AUTHORS:        Adam Antinoo - haddockgit@gmail.com
//  COPYRIGHT:      (c) 2013-2014 by Dimensinfin Industries, all rights reserved.

package org.dimensinfin.evedroid.core;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum EDirectorCode {
	BPODIRECTOR, MARKETDIRECTOR, STOCKDIRECTOR, JOBDIRECTOR, LOCATIONDIRECTOR, ASSETDIRECTOR, TASKDIRECTOR, MININGDIRECTOR, MODULEDIRECTOR, FITDIRECTOR, INDUSTRYDIRECTOR
}

// - UNUSED CODE ............................................................................................
