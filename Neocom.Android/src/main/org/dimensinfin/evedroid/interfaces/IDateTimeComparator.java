//  PROJECT:        EIA
//  AUTHORS:        Adam Antinoo - haddockgit@gmail.com
//  COPYRIGHT:      (c) 2013-2014 by Dimensinfin Industries, all rights reserved.

package org.dimensinfin.evedroid.interfaces;

// - IMPORT SECTION .........................................................................................
import org.joda.time.DateTime;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IDateTimeComparator {
	// - M E T H O D - S E C T I O N ..........................................................................
	public DateTime getComparableDate();
}

// - UNUSED CODE ............................................................................................
