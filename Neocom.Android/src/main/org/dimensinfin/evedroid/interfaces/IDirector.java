//PROJECT:        EveDroid
//AUTHORS:        Adam Antinoo - adamantinoo.git@gmail.com
//COPYRIGHT:      (c) 2013-2014 by Dimensinfin Industries, all rights reserved.

package org.dimensinfin.evedroid.interfaces;

import org.dimensinfin.eveonline.neocom.model.NeoComCharacter;

// - CLASS IMPLEMENTATION ...................................................................................
public interface IDirector {
	public abstract boolean checkActivation(NeoComCharacter pilot);

	//	public abstract String getDescription();
	//
	//	public abstract int getIconReference();
	//
	//	public abstract String getName();
}

// - UNUSED CODE ............................................................................................
